﻿namespace Orchard.Logic.Core
{
    public class SnapCracklePop
    {
        public string GetResult()
        {
            var result = string.Empty;
            for (var i = 1; i <= 100; i++)
            {
                
                if (i % 3 == 0 && i % 5 == 0)
                {
                    result += "SNAP";
                }
               else if (i % 3 == 0)
                {
                    result += "CRACKLE";
                }
              else if (i % 5 == 0)
                {
                    result += "POP";
                }
                else
                {
                    result += i.ToString();
                }
            }
            return result;
        }
    }
}
