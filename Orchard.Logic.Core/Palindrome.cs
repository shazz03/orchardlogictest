﻿using System.Text.RegularExpressions;

namespace Orchard.Logic.Core
{
    public class Palindrome
    {
        public string IsPalindrome(string input)
        {
            if (string.IsNullOrEmpty(input))
                return "UNDETERMINED";

            input = new Regex("[^a-zA-Z]").Replace(input, "").ToLower();
            var length = input.Length;
            for (var i = 0; i < length / 2; i++)
            {
                if (input[i] != input[length - i - 1])
                    return "FALSE";
            }
            return "TRUE";
        }
    }
}
