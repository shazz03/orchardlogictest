﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Orchard.Logic.Core
{
    public class InstaGram
    {
        public string Convert(string input)
        {
            var dictOfChar = new Dictionary<string, int>();
            //remove special characters
            input = new Regex("[^a-zA-Z]").Replace(input, "").ToLower();
            foreach (var c in input)
            {
                //check if dictionary contains key of charcter
                if (dictOfChar.ContainsKey(c.ToString()))
                {
                    //increment value
                    dictOfChar[c.ToString()]++;
                }
                else
                {
                    //add new 
                    dictOfChar.Add(c.ToString(), 1);
                }
            }
            string output;
            var firstValue = dictOfChar.Values.FirstOrDefault();
            //check if input length is equal to dictionary of character with value of 1
            //this will check for character only occurred once
            if (input.Length == dictOfChar.Count(w => w.Value == 1))
            {
                output = "HETEROGRAM";
            }
            //check if input length is equal to dictionary of character with value of its first value multiply by first value 
            //this will check for equal number of characters
            else if (input.Length == dictOfChar.Count(w => w.Value == firstValue) * firstValue)
            {
                output = "ISOGRAM";
            }
            else
            {
                output = "NOTAGRAM";
            }
            return output;
        }
    }
}
