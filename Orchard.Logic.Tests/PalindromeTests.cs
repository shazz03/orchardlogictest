﻿using System;
using NUnit.Framework;
using Orchard.Logic.Core;

namespace Orchard.Logic.Tests
{
    [TestFixture]
    public class PalindromeTests
    {
        [TestCase("emordnilaP","FALSE")]
        [TestCase("", "UNDETERMINED")]
        [TestCase("Kayak", "TRUE")]
        [TestCase("No lemon, no melon", "TRUE")]
        [TestCase("A TOYOTA'S A TOYOTA", "TRUE")]
        [TestCase("ESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED","FALSE")]
        [TestCase("DETARTRATED", "TRUE")]
        public void PalindromeTest(string input, string valid)
        {
            var pal = new Palindrome();

            Assert.AreEqual(valid, pal.IsPalindrome(input));
        }
    }
}
