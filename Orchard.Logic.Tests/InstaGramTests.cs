﻿using NUnit.Framework;
using Orchard.Logic.Core;

namespace Orchard.Logic.Tests
{
    [TestFixture]
    public class InstaGramTests
    {
        [TestCase("uncopyrightable", "HETEROGRAM")]
        [TestCase("Disco", "HETEROGRAM")]
        [TestCase("Inside", "NOTAGRAM")]
        [TestCase("DIVIVD", "ISOGRAM")]
        [TestCase("Caucasus", "ISOGRAM")]
        [TestCase("authorising", "NOTAGRAM")]
        public void InstaGram_Check_If_It_Is_Any_Gram(string input, string output)
        {
            var gram = new InstaGram();
            var result =gram.Convert(input);
            Assert.AreEqual(output, result);
        }
    }
}
